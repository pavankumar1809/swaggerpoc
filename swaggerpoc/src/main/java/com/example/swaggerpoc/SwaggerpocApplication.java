package com.example.swaggerpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerpocApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwaggerpocApplication.class, args);
	}
}
